# A django cms project to be deployed in heroku

Initialize a virtualenv for the project

	virtualenv venv
	source venv/bin/activate

run the 'source' command everytime you are going to work on the project

Install dependencies

	pip install -r requirements.txt

## Local development

To be able to run the project locally with foreman create a file named .env and add a line like the following:

	DATABASE_URL=postgres://<dbuser>:<password>@<host>/<dbname>

replace the values between <> with the proper values of your postgresql installation

Then run (only the first time):

	foreman run python manage.py syncdb --all
	foreman run python manage.py migrate --fake

On upgprades run:

	foreman run python manage.py syncdb
	foreman run python manage.py migrate

## Heroku deployment

After a git push of your project on Heroku run the following commands:

The first time:

	heroku run python manage.py syncdb --all
	heroku run python manage.py migrate --fake

On upgrades run:

	heroku run python manage.py syncdb
	heroku run python manage.py migrate

## Message translations (i18n)

Localization of string messages

Create a locale directory in the same directory where manage.py is located

To create the message files for the transalations use the following command:

	django-admin.py makemessages -l <lang> --ignore 'venv'

where <lang> should be a language (en, es, de, fr)

WARNING: Be sure to review the # fuzzy comments in the created files and remove them once reviwed

To compible the translation files run

	django-admin.py compilemessages
